FROM gricad-registry.univ-grenoble-alpes.fr/osug/dc/shiny-base:4.0.2

# Install dependencies
RUN install.r \
      foreach \
      shinyWidgets \
      zip \
      reshape \
      && rm -rf /tmp/downloaded_packages

# Copy app files
COPY --chown=shiny app.R /srv/shiny/
COPY --chown=shiny DATA/ /srv/shiny/DATA
